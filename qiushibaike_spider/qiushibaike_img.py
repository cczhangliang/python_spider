#coding=utf-8

import re
import urllib2

import os
import urlparse

from bs4 import BeautifulSoup


def get_html(url):
    user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
    headers = {'User-Agent': user_agent}
    req = urllib2.Request(url, headers=headers)
    response = urllib2.urlopen(req)
    return response.read()

def get_img(html):
    soup = BeautifulSoup(html, 'html.parser')#, from_encoding='utf-8'
    links = soup.find_all('img', src=re.compile(r"//pic.qiushibaike.com/system/pictures/"))
    return links

html=get_html('https://www.qiushibaike.com/imgrank/').decode('utf-8')
images_urls = get_img(html)

if os.path.exists("G:/pywork/imags") == False:
    os.mkdir("G:/pywork/imags/")

count = 1
for url in images_urls:
    print url
    full_url = urlparse.urljoin('https:', url['src'].split('?')[0])
    if(full_url.find('.') != -1):
        name = full_url.split('/')[-1]
        print "name",name
        req = urllib2.Request(full_url)
        response = urllib2.urlopen(req)
        f = open("G:/pywork/imags/"+url['alt']+"_"+name,'wb')
        f.write(response.read())
        f.flush()
        f.close()
        count += 1




