#coding=utf-8
import cookielib
import urllib2

from bs4 import BeautifulSoup

raw_cookies='''bid:y7wIRhBJQOQ;_pk_id.100001.4cf6:bb7a7bb337e78f58.1510647529.1.1510647580.1510647529.;_pk_ses.100001.4cf6:*;__utma:223695111.493235935.1510647531.1510647531.1510647531.1;__utmb:223695111.0.10.1510647531;__utmc:223695111;__utmz:223695111.1510647531.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none);as:"https://movie.douban.com/subject/6982558/comments?status=P";ps:y;dbcl2:"156074167:1uTNltookr8";ck:-A5T;push_noty_num:0;push_doumail_num:0'''

cookies = cookielib.CookieJar()

for line in raw_cookies.split(';'):
    #print line.split(':',1)#1代表只分一次，得到两个数据
    key,value = line.split(':',1)
    cookies[key] = value

#发送请求头，爬取所需要的信息
def getInfo(url):
    page_num = 0
    Infolist = []
    headers = dict()
    headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    headers['Accept-Encoding'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    headers['Ac-Language'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    headers['Connection'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    headers['Host'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    headers['Referer'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    headers['User-agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'

    get_url =url #利用cookie请求访问另一个网址
    handler = urllib2.HTTPCookieProcessor(cookies)
    opener = urllib2.build_opener(handler)
    urllib2.install_opener(opener)

    req = urllib2.Request(get_url,headers=headers)

    response = urllib2.urlopen(req)
    print response

    soup = BeautifulSoup(response.read(),'lxml')

    comments = soup.find_all("div",class_='comment-item')

    for i in comments:
        print i['class']

#主函数
if __name__ == '__main__':
    getInfo('https://movie.douban.com/subject/6982558/comments?status=P')





